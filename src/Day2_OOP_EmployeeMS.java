import org.junit.jupiter.api.*;

import java.util.*;
import java.util.Map;
import java.util.HashMap;
public class Day2_OOP_EmployeeMS {
    private static Employee user_create(ArrayList<String> data, int i){
        String id = data.get(i);
        String name = data.get(i+1);
        String department = data.get(i+2);
        double salary = Double.parseDouble(data.get(i+3));
        //System.out.println(i +" : "+ id + name + department + salary);
        Employee employee = new Employee(id, name, department, salary);
        return employee;
    }
    private static List<Department> department_create(List<Department> departments,List<Employee> employees){
        for(Employee e : employees){
            boolean flag = true;
            for(int i = 0; i < departments.size();i++) {
                //System.out.println("Comparing : "+ departments.get(i).getD_name() + " : " +e.get_Department());
                if (departments.get(i).getD_name().equals(e.get_Department())) { // Framework in Map : ContainsKey already in the map Something like using
                    departments.get(i).addNewEmployees(e);
                    System.out.println("Adding employee");
                    flag = false;
                    break;
                }
            }
            if(flag){
                Department department = new Department(e.get_Department(), e);
                departments.add(department);
            }
        }

        return departments;
    }


    @Test
    public static void main(String[] args) {
        //Adding simple
        //Employee e1 = new Employee();
        String path = "C:/Users/zenasc/IdeaProjects/DailyPactice/Resource/employee_data.csv";
        String path2 = "C:/Users/zenasc/Downloads/week1/Day3/employee_data.csv";

        Readcsv rc = new Readcsv();
        rc.setPath(path);

        List<Employee> employees = new ArrayList<>();
        List<Department> departments = new ArrayList<>();
        Map<Department, List<Employee>> departments_List = new HashMap<>();

        ArrayList<String> userData = rc.getCsvData();
        rc.setPath(path2);
        userData.addAll(rc.getCsvData());

        for(int i = 0; i < userData.size(); i = i + 4 ) {
            employees.add(user_create(userData, i));
        }

        departments = department_create(departments,employees);

        //System.out.println(departments);
        for(Department d : departments){
            System.out.println(d.getD_name());
            d.viewNameList_All_Details();
        }


        for(Department d : departments){
            if(departments_List.containsKey(d)){

            }
            else{
                departments_List.put(d,d.getEmployees());
            }
        }
        System.out.println(departments_List);


    }
}
//Develop