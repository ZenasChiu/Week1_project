import java.util.*;

public class Day4_mapping {
    public static void main(String[] args) {
        Map<String, Integer> wordCount = new HashMap<>(); // Key and value
        Map<String, ArrayList<String>> human = new HashMap<>();
        String[] words = {"apple", "banana", "orange", "apple", "banana", "grape", "orange"};

        for (String word : words) {
            if (wordCount.containsKey(word)) { // Framework in Map : ContainsKey already in the map Something like using
                //System.out.println(wordCount.get(word));
                int count = wordCount.get(word);
                count++;
                wordCount.put(word, count); // Creating new pairs of keys and value
            } else {
                //System.out.println(wordCount.get(word)); // not same as Arraylist get --> this only get the value by the keys
                wordCount.put(word, 1);
            }
        }

        System.out.println(wordCount.keySet()); // [banana, orange, apple, grape] Print out the "Key"set of information
        for (String word : wordCount.keySet()) {    // keySet() method th

            int count = wordCount.get(word);    // It's that means the String part have become a unqule ID ?
            System.out.println(word + ": " + count);
        }
    }
}
