import org.junit.jupiter.api.*;

import java.util.ArrayList;
import java.util.List;
public class Department {
    private String d_name;
    private List<Employee> employees;

    Department(String d_name, List employees) {
        this.d_name = d_name;
        this.employees = employees;
    }
    Department(String d_name, Employee e){
        System.out.println("Adding new department");
        this.d_name = d_name;
        this.employees = new ArrayList<>();
        addNewEmployees(e);

    }

    public String getD_name() {
        return d_name;
    }

    public List getEmployees(){
        return employees;
    }
    public void changeDepartmentname(String newName) {
        this.d_name = newName;
    }

    public void addNewEmployees(Employee employee){
        employees.add(employee);
        employee.setDeparment(getD_name());
    }
    public void removeEmployees(Employee employee){
        employees.remove(employee);
        employee.setDeparment(null);
    }

    public void viewNameList(){
        for(Employee e : employees){
            System.out.printf("Name : "+e.get_name());
        }
    }
    public void viewNameList_All_Details(){
        for(Employee e : employees){
            String details = String.format("ID: %s |Name: %s \t\t|Salary: %f \t|Department : %s",e.getE_Id(),e.get_name(),e.get_salary(),e.get_Department());
            System.out.println(details);
        }
    }


}
