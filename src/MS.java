class testingforinterface{
    String number;
}

interface A {
    void a();
    void b();
}
abstract class AbsClass implements A{
    public void b(){
        System.out.println("hello b~");
    }
    abstract void c();
}
class MyClass extends AbsClass{

    public void a() {
        System.out.println("a void from My class Overriding interface A");
    }
    // 方法b()已經在AbsClass實作，MyClass不需要再實作，當然也可以再覆寫b()
    public void c() {
        System.out.println("c from from My class Overriding Abstract class ABS");
    }

}

public class MS extends MyClass{
    public static void main(String[] args){
        AbsClass m = new MyClass();
        AbsClass A = new AbsClass() {
            @Override
            void c() {
                System.out.println("New C from new A overriding Self class");
            }
            @Override
            public void a() {
                System.out.println("New A from new A overriding Self interface A");
            }
        };

        callOut(A);
        callOut(m);


    }
    public static void callOut(AbsClass abc){
        abc.a();
        abc.b();
        abc.c();
        //Both can print beacuase of the same ABS class
    }

}