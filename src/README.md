Recording the practice of week 1 

# Day 1

### Quiz 1
  - Print 
  - Looping using "for", "while"
  - Encoding, Regex

# Day 2 

### OOP 
  - Public, Private , Class object
  - Hash map maping 
  - file scan read write 
  - Try catch
  - exception vs IOE exception
- Set up GitHub
  - First push done

# Day 3

### Employee system updated 
  - Adding auto read csv --> create class

### Java
  - Abstract vs Interface vs Overriding / Overloading
  - Array Fixed Size / framework : Arraylist dynamic size
  - Extend Class : xx(Child) Extends xx(super)
  - After lunch --> 3 links reading and try
  - Hashmap usage of student details : https://www.geeksforgeeks.org/create-hashmap-with-multiple-values-associated-with-the-same-key-in-java/
  - Good handle for exception : https://howtodoinjava.com/best-practices/java-exception-handling-best-practices/
  - Debug tips https://stackify.com/java-debugging-tips/

### OOP JAVA --> https://yubin551.gitbook.io/java-note/object_oriented_programming/inheritance
- Abstraction (Class) / Interface(muti implement + extends interface)
  - Abstract class require to override 
  - Interface provide Variable "public static final"
- Encapsulation 封裝
  - Basic class usage 
    - Hiding Data with private / public
    - Setting Setter() / Getter() for each object

- Inheritance 繼承
  - AVD: Increase Reuseablity from Super class
  - Child class "Extend" from Parents class
  - More like a category to smaller category or object 
  - "this." = Self box name 
  - "super." = Upper box name
  - Only Getting from Upper , No Lower getting

- Polymorphism 多型
  - Base on Inheritance "Extends"
  - Class can be created with same Big Box
- Example:

      AbsClass m = new MyClass(); 
      AbsClass A = new AbsClass();

### Extra Inform
  - Both will count as AbsClass while calling
  - Can also call separately
  - Downcasting / Upcasting
  - Usage : not really

### JAVA Extra information
- clone() Usage
  - Cloning all details from selected Object
    - Same Class Based
    - Same Overloading required


    public Object clone()throws CloneNotSupportedException{
        return super.clone();
    }
    Try{
      A a = new A();
      A b = (A)a.clone();
    }catch(CloneNotSupportedException c){}
    // a.details = b.details


  
    
        
### Data and time
- api : java.time.LocalDate
- website : https://www.javatpoint.com/java-date

- ArrayList /LinkList
  - In Java LinkedList class, manipulation is fast because no shifting needs to occur.
      
- Set / Map
- List / HastSet/ HastMap / EnumSet / EnumMap
- Static Binding and Dynamic Binding

- Algorithm : https://morosedog.gitlab.io/algorithm-20200305-sort-0/
- Algorithm Part1 : https://blog.csdn.net/roger_coderlife/article/details/100129999
- Algorithm Part2 : https://blog.csdn.net/qq_42453117/article/details/100036347

  
- GitLab setup
  - Finally sync with intelliJ --> Download + new Branches 
  - https://gitlab.com/ZenasChiu/Week1_project/-/tree/development/src
    
# Day4
- MS system update 
  -  Added Departments{ name and employee lists}
  -  readcsv path can be editable
  
- JAVA
  -  map <Key ,value> new Hashmap
      - containKey() compare the key is contain in the map
  -  Hashset<e>
    - Doesn't allow duplicate elements 
    - List <value> new Arraylist
      - contain() compare something that same value with the List 

    - Enum : https://www.tpisoftware.com/tpu/articleDetails/1432
      - special class that used for storing fixed information

    - JUnit Test :https://www.jetbrains.com/help/idea/junit.html#intellij
      - Using for testing the algorithm mostly
      
    - MVC : https://ithelp.ithome.com.tw/articles/10191216
      - Design pattern - Model View Controller
  

- Calculator / CalculatorTest : using for Junit test case

# Day5

### JAVA Quiz 2 
- Testing transfering csv to JSON file
- Main point : 
  - Only using Packet : java.io.* / java.util.*
  - CSV set with lines and "," 
  
#### Reading line to line in 
    ```
         BufferedReader reader = new BufferedReader(new FileReader(this.path));
    ```

#### Splitting the String

    ```
         String[] line_split = line.split(",");
    ```
#### Checking For String that contain " " double quote
-  There are two ways 1 : Simple scan and replace
  
    ```
      // Check if the string contains double quotes
      boolean hasQuotes = str.indexOf('"') >= 0;
    
      if (hasQuotes) {
          // Remove the double quotes using replace
          str = str.replace("\"", "");
      }
    ```

  - There are two ways 2 : Check for mixed String with " " double quotes and Combat the String in same item
    - Explain : Since there are case : ** "My name is "Zenas ,Chiu" " **
    - Where the String contain double quotes and "," that will mess up the code

    ```
        private String temp_header;                         //Temple hold the String Head
        private int category_num = 0;                       //For count the number of item

        private static final String header_REGEX = "^\".*"; //Regex for Start
        private static final String tail_REGEX = ".*\"$";   //Regex for End 

        public static boolean check_header(String word) {   //Check String Start with "
            Pattern pattern = Pattern.compile(header_REGEX);
            Matcher matcher = pattern.matcher(word);
            return matcher.matches();
        }
        public static boolean check_tail(String word) {     //Check String end with "
            Pattern pattern = Pattern.compile(tail_REGEX);
            Matcher matcher = pattern.matcher(word);
            return matcher.matches();
        }
        private static boolean hasQutoes(String word){      //Check extra Qutoes
            return word.indexOf('"') >= 0;
        }


        private ArrayList<String> load_csv() {
            try {
                boolean flag = true;                        //flag that start/end stack the Splitted String with double quotes
                BufferedReader reader = new BufferedReader(new FileReader(this.path));
                System.out.println("readable file");//Debug return
                String line;

                while ((line = reader.readLine()) != null)                                      //Read the csb Line by Line 
                {

                    String[] line_split = line.split(",");
                    if(this.category_num == 0){this.category_num = line_split.length;}

                    for(String temple_Word_holder : line_split){                                //loop every split item from each line

                        if (check_header(temple_Word_holder)) {
                            if(flag = true){                                                    //adding the deleted ","
                                this.temp_header = this.temp_header+ temple_Word_holder;        //First Stack
                            }else{
                                this.temp_header = this.temp_header+","+ temple_Word_holder;    //Next Stack
                            }
                            flag = false;                                                       //Start stack the String

                        }
                        if (check_tail(temple_Word_holder)) {
                            flag = true;                                                        //End stack the String
                            temple_Word_holder = this.temp_header+","+ temple_Word_holder;
                            if(hasQutoes(temple_Word_holder)){
                                temple_Word_holder=temple_Word_holder.replace("\"", ""); // Removing the extra quotes in the String
                            }
                        }
                        if (flag) {                                                             //Storing the information into ArrayList

                            this.data.add(temple_Word_holder);
                            temp_header = "";                                                   //Reset the header
                        }
                    }
                }

                reader.close();
                return this.data;

                } catch (Exception e) {
                    System.out.println("File Can;t read");
                    return null;
                }
            }
    ```

For Getting a Arraylist --> Map<String, Arraylist<String>> --> Create Json file



- MVC : 
  - Model 模型
    
  - View 視圖
    
  - Controller 控制器


