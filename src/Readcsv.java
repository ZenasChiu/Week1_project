import java.io.*;
import java.io.BufferedReader;
import java.util.ArrayList;
import java.util.regex.*;

public class Readcsv {
        private  ArrayList<String> data = new ArrayList<String>();
        private String path = "";
        private String temp_header;                         //Temple hold the String Head
        private int category_num = 0;                       //For count the number of item

        private static final String header_REGEX = "^\".*"; //Regex for Start
        private static final String tail_REGEX = ".*\"$";   //Regex for End

        public static boolean check_header(String word) {   //Check String Start with "
            Pattern pattern = Pattern.compile(header_REGEX);
            Matcher matcher = pattern.matcher(word);
            return matcher.matches();
        }
        public static boolean check_tail(String word) {     //Check String end with "
            Pattern pattern = Pattern.compile(tail_REGEX);
            Matcher matcher = pattern.matcher(word);
            return matcher.matches();
        }
        private static boolean hasQutoes(String word){      //Check extra Qutoes
            return word.indexOf('"') >= 0;
        }


        private ArrayList<String> load_csv() {
            try {
                boolean flag = true;                                //flag that start/end stack the Splitted String with double quotes
                BufferedReader reader = new BufferedReader(new FileReader(this.path));
                System.out.println("readable file");//Debug return
                String line;

                while ((line = reader.readLine()) != null)                                      //Read the csb Line by Line
                {

                    String[] line_split = line.split(",");
                    if(this.category_num == 0){this.category_num = line_split.length;}

                    for(String temple_Word_holder : line_split){                                //loop every split item from each line

                        if (check_header(temple_Word_holder)) {
                            if(flag = true){                                                    //adding the deleted ","
                                this.temp_header = this.temp_header+ temple_Word_holder;        //First Stack
                            }else{
                                this.temp_header = this.temp_header+","+ temple_Word_holder;    //Next Stack
                            }
                            flag = false;                                                       //Start stack the String

                        }
                        if (check_tail(temple_Word_holder)) {
                            flag = true;                                                        //End stack the String
                            temple_Word_holder = this.temp_header+","+ temple_Word_holder;
                            if(hasQutoes(temple_Word_holder)){
                                temple_Word_holder=temple_Word_holder.replace("\"", ""); // Removing the extra quotes in the String
                            }
                        }
                        if (flag) {                                                             //Storing the information into ArrayList

                            this.data.add(temple_Word_holder);
                            temp_header = "";                                                   //Reset the header
                        }
                    }
                }

                reader.close();
                return this.data;

                } catch (Exception e) {
                    System.out.println("File Can;t read");
                    return null;
                }
            }


        public void setPath(String path){this.path = path;}

        public ArrayList<String> getCsvData(){
            return this.load_csv();
        }
        public int getCategory_num(){
            return this.category_num;
        }
}
