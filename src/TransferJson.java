import java.io.IOException;
import java.io.FileWriter;
import java.util.*;


public class TransferJson { //Transfer ArrayList to Hashmap to JSON
 private ArrayList<String> arrayList_data;
 private int category_Num;
 private int json_size;
 private String path;
 private Map<String,ArrayList<String>> json_data = new HashMap<>();
    private void transfer_ArrayList_Hashmap(){
            int count=0;
            String[] key = new String[category_Num];
            for(int i = 0 ; i < this.category_Num; i++){
                this.json_data.put(arrayList_data.get(0),new ArrayList<String>());
                key[i] = arrayList_data.get(0);
                this.arrayList_data.remove(arrayList_data.get(0));
            }

            for(String temple_Word_Holder : arrayList_data){
                System.out.println(temple_Word_Holder);
                    if(count < category_Num){
                        json_data.get(key[count]).add(temple_Word_Holder);
                    }
                    else{
                        count = 0;
                        json_data.get(key[count]).add(temple_Word_Holder);
                        json_size++;
                    }
                count++;
            }
            System.out.println(json_data);
            System.out.println(json_size);
    }

    public void setArrayList_data(ArrayList<String> arrayList_data , int category_Num) {
        this.arrayList_data = arrayList_data;
        this.category_Num = category_Num;
    }

    public void createJson(String path) throws IOException{
        this.path = path;
        transfer_ArrayList_Hashmap();


        String file_name = path.replaceAll(".*/|\\.[^.]*$", "");
        String newFilePath = path.replace(".csv", ".json");

        StringBuilder sb = new StringBuilder();
        sb.append("[");

            for(int i = 0; i <=json_size; i ++) {
                sb.append("{");
                for (String key : this.json_data.keySet()) {
                    try{
                        sb.append("\"").append(key).append("\":\"").append(this.json_data.get(key).get(i)).append("\",");
                    }
                    catch (IndexOutOfBoundsException e){
                        sb.append("").append("\",");
                    }
                }
                sb.deleteCharAt(sb.length() - 1); // Remove the last comma
                if(i == json_size){sb.append("}");}
                else{sb.append("},");}
            }
        sb.append("]");

        //System.out.println(sb);

        FileWriter fileWriter = new FileWriter(newFilePath);
        fileWriter.write(sb.toString());
        fileWriter.close();
    }

}
